const withPlugins = require('next-compose-plugins');
const optimizedImages = require('next-optimized-images');
const webpack = require('webpack')
const path = require('path')

const nextConfiguration = {
  images: {
    loader: 'akamai',
    disableStaticImages: true,
    path: '/'
  },
  target: 'serverless', //will output independent pages that don't require a monolithic server. It's only compatible with next start or Serverless deployment platforms (like ZEIT Now) — you cannot use the custom server API.
};

module.exports = withPlugins([optimizedImages, {
    imagesFolder: 'images',
    imagesName: '[name].[ext]',
    handleImages: ['jpg','jpeg', 'png','svg', 'webp', 'gif'],
}], nextConfiguration);
