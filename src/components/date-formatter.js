import moment from 'moment'

export default function DateFormatter({ dateString }) {
  return <time dateTime={dateString}>{moment(dateString).format("DD MMMM YYYY")}</time>
}
