/** @jsxRuntime classic */
/** @jsx jsx */

import { jsx, Box } from "theme-ui";

export default function Avatar({ name, picture }) {
  return (
    <Box sx={styles.container}>
      <img src={picture} className="w-12 h-12 rounded-full mr-4" alt={name} />
      <div className="text-xl font-bold">{name}</div>
    </Box>
  )
}

const styles = {
  container:{
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    color: "#9a9a9a",
    marginTop: "20px",
    marginBottom: "20px",
    img: {
      width: '30px',
      height: '30px',
      borderRadius: '50%',
      boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',  
      marginRight: '10px'
    }
  }
}