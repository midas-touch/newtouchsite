import {useEffect} from 'react';

export function GoogleAd() {
  useEffect(() => {
    try {
      (window.adsbygoogle = window.adsbygoogle || []).push({});
    } catch (err) {
      console.error(err);
    }
  }, []);

  return (
    <ins
      style={{display: 'block'}}
      data-ad-format="fluid"
      data-ad-layout-key="-g8-1l-1o+6+gd"
      data-ad-client="ca-pub-8294995671791919"
      data-ad-slot="6462995672"
    />
  );
}
