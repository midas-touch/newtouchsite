import React from "react";
import Head from "next/head";

export default function SEO({
  description = `At Midas Touch Technologies, we specialize in transforming your business ideas into digital gold. As a leading web application development company based in South Africa, we're dedicated to empowering small businesses like yours with tailor-made solutions that drive growth and efficiency.`,
  author = "Midas Touch Technologies",
  meta,
  title = "Midas Touch Technologies",
}) {
  const metaData = [
    {
      name: `description`,
      content: description,
    },
    {
      name: `og:title`,
      content: title,
    },
    {
      name: `og:description`,
      content: description,
    },
    {
      name: `og:type`,
      content: `website`,
    },
    {
      name: `og:image`,
      content: `https://i.ibb.co/KjZzpwR/xyz.png`,
    },
    {
      name: `twitter:card`,
      content: `summary`,
    },
    {
      name: `twitter:creator`,
      content: author,
    },
    {
      name: `twitter:title`,
      content: title,
    },
    {
      name: `twitter:description`,
      content: description,
    },
  ].concat(meta);
  return (
    <Head>
      <title>{title}</title>
      <meta
        name="google-site-verification"
        content="f3R768bYyyNWIu6nlTxcwc709_ou-eHp7Ui0pYxeDc8"
      />
      {metaData.map(({ name, content }, i) => (
        <meta key={i} name={name} content={content} />
      ))}
    </Head>
  );
}

SEO.defaultProps = {
  lang: `en`,
  meta: [],
};
