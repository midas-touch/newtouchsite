import Chat from '../../assets/widget/chat.svg';
import Community from '../../assets/widget/community.svg';
import Github from '../../assets/widget/github.svg';

export default {
  widgets: [
    {
      id: 1,
      iconSrc: Chat,
      altText: 'Community',
      title: 'Join the Conversation',
      description:
        <p>If you struggling to figure out if you need a web app or not drop us an email at <a href="mailto:info@touch.net.za">info@touch.net.za</a></p>,
    },
    {
      id: 2,
      iconSrc: Community,
      altText: 'Chat',
      title: 'Need help?',
      description:
      <p>If you need help with an existing web application drop us an email at <a href="mailto:help@touch.net.za">help@touch.net.za</a></p>,
    },
    {
      id: 3,
      iconSrc: Github,
      altText: 'Github',
      title: 'View our code',
      description:
      <p>If you would like to view some of our open source code visit <a href="https://gitlab.com/midas-touch">this link</a></p>,
    },
  ],
  menuItem: [
    {
      path: '/',
      label: 'Home',
    },
    {
      path: '/',
      label: 'Adversite',
    },
    {
      path: '/',
      label: 'Supports',
    },
    {
      path: '/',
      label: 'Marketing',
    },
    {
      path: '/',
      label: 'Contact',
    },
  ],
};
