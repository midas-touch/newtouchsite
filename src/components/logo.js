/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx, Image } from 'theme-ui';
import { Link } from './link';

export default function Logo({ src, ...rest }) {
  return (
    <Link
      path="/"
      sx={{
        variant: 'links.logo',
        display: 'flex',
        cursor: 'pointer',
        mr: 15,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        minWidth: "150px",
        textDecoration: "none",
        color: "inherit",
        fontWeight: "900",
      }}
      {...rest}
    >
      <Image sx={{width: "40px;"}} src={src} alt="startup landing logo" />
      Midas Touch
    </Link>
  );
}
