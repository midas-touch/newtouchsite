/** @jsxRuntime classic */
/** @jsx jsx */


import { jsx, Box} from "theme-ui";
import Avatar from '../components/avatar'
import DateFormatter from '../components/date-formatter'
import CoverImage from '../components/cover-image'
import Link from 'next/link'

export default function HeroPost({
  title,
  coverImage,
  date,
  excerpt,
  author,
  slug,
}) {
  return (
    <section sx={styles.postBox}>
      <div>
        <CoverImage
          title={title}
          src={coverImage}
          slug={slug}
          height={200}
          width={400}
        />
      </div>
      <div>
        <div>
          <h3>
            <Link as={`/posts/${slug}`} href="/posts/[slug]">
              <a className="hover:underline">{title}</a>
            </Link>
          </h3>
          <div>
            <DateFormatter dateString={date} />
          </div>
        </div>
        <div>
          <p>{excerpt}</p>
          <Avatar name={author.name} picture={author.picture} />
        </div>
      </div>
    </section>
  )
}

const styles = {
  postBox: {
    width: "350px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    padding: "15px",
    margin: "10px",
    borderRadius: "10px",
    boxShadow: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
    a:{
      color: "inherit",
      textDecoration: "none",
    },
    h3:{
      ":hover":{
        a:{
          color: "primary",
          textDecoration: "none",
        }
      }
    }
  },

};