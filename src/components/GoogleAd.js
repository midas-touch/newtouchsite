import {useEffect} from 'react';

export function GoogleAd() {
  useEffect(() => {
    try {
      (window.adsbygoogle = window.adsbygoogle || []).push({});
    } catch (err) {
      console.error(err);
    }
  }, []);

  return (
    <ins
      className="adsbygoogle"
      style={{display: 'block'}}
      data-ad-client="ca-pub-8294995671791919"
      data-ad-slot="1763518552"
      data-ad-format="auto"
      data-full-width-responsive="true"
    />
  );
}
