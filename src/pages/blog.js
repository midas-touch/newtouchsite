import Layout from "../components/layout";
import { getAllPosts } from "../lib/api";
import Head from "next/head";
import { ThemeProvider, Container } from "theme-ui";
import { StickyProvider } from "../contexts/app/app.provider";
import theme from "../theme";
import HeroPost from '../components/hero-post'

export default function Index({ allPosts }) {
  const heroPost = allPosts[0];
  const morePosts = allPosts.slice(1);
  return (
    <ThemeProvider theme={theme}>
      <StickyProvider>
        <Layout isOutsideHome>
          <Container sx={styles.container}>
          <Head>
            <title>Next.js Blog Example with</title>
          </Head>
          {heroPost && (
            <HeroPost
              title={heroPost.title}
              coverImage={heroPost.coverImage}
              date={heroPost.date}
              author={heroPost.author}
              slug={heroPost.slug}
              excerpt={heroPost.excerpt}
            />
          )}
          {morePosts.map((post) => (
            <HeroPost
              title={post.title}
              coverImage={post.coverImage}
              date={post.date}
              author={post.author}
              slug={post.slug}
              excerpt={post.excerpt}
            />
          ))}
          
          </Container>
        </Layout>
      </StickyProvider>
    </ThemeProvider>
  );
}

const styles = {
  container: {
    pt: ['140px', '145px', '155px', '80px', null, null, '100px', '215px'],
    pb: [2, null, 0, 1, 2, 0, null, 5],
    position: 'relative',
    zIndex: 2,
    maxWidth: "1000px",
    display: "flex",
    justifyContent: "center",
    alignItems: "start",
    flexWrap: "wrap",

  },
};

export async function getStaticProps() {
  const allPosts = getAllPosts([
    "title",
    "date",
    "slug",
    "author",
    "coverImage",
    "excerpt",
  ]);

  return {
    props: { allPosts },
  };
}
