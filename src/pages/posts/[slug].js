/** @jsxRuntime classic */
/** @jsx jsx */

import React from "react";
import { useRouter } from "next/router";
import ErrorPage from "next/error";
import PostBody from "../../components/post-body";
import Header from "../../components/header";
import PostHeader from "../../components/post-header";
import Layout from "../../components/layout";
import { Container, ThemeProvider, jsx, Box } from "theme-ui";
import theme from "../../theme";
import { StickyProvider } from "../../contexts/app/app.provider";
import { getPostBySlug, getAllPosts } from "../../lib/api";
import PostTitle from "../../components/post-title";
import Head from "next/head";
import markdownToHtml from "../../lib/markdownToHtml";
import { GoogleAd } from "../../components/GoogleAd";


export default function Post({ post, morePosts, preview }) {
  const router = useRouter();
  if (!router.isFallback && !post?.slug) {
    return <ErrorPage statusCode={404} />;
  }
  return (
    <ThemeProvider theme={theme}>
      <StickyProvider>
        <Layout preview={preview} isOutsideHome>
          <Container sx={styles.container}>
            <Box sx={styles.contentBox}>
              {router.isFallback ? (
                <PostTitle> Loading… </PostTitle>
              ) : (
                <>
                  <article>
                    <Head>
                      <title>{post.title} | Midas Touch Technologies</title>
                      <meta property="og:image" content={`https://www.touch.net.za${post.ogImage.url}`} />
                    </Head>
                    <PostHeader
                      title={post.title}
                      coverImage={post.coverImage}
                      date={post.date}
                      author={post.author}
                    />
                    <PostBody content={post.content} />
                    <GoogleAd />
                  </article>
                </>
              )}
            </Box>
          </Container>
        </Layout>
      </StickyProvider>
    </ThemeProvider>
  );
}

const styles = {
  
  container: {
    pt: ['80px', '145px', '155px', '80px', null, null, '100px', '215px'],
    pb: [2, null, 0, 1, 2, 0, null, 5],
    position: 'relative',
    zIndex: 2,
    maxWidth: "900px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  contentBox:{
    img: {
      maxWidth: '100%'
    },
    maxWidth: "900px",
    blockquote:{
      background: "#efefef",
      padding: "10px",
      borderLeft: "4px solid #c33e51",
      borderRadius: "10px",
    },
    code:{
      background: "#060606",
      color: "#dc9af3",
      padding: "2.5px 5px 2.5px 5px",
      borderRadius:" 5px"
    },
    pre:{
      background: "#060606",
      color: "#dc9af3",
      padding: "5px",
      borderRadius: "5px",
      padding:"20px",
      whiteSpace: "pre-wrap",
      wordWrap: "break-word",
      textAlign: "justify",
      code: {
        fontSize:["8px", "14px"]
      }
    }
  }

};


export async function getStaticProps({ params }) {
  const post = getPostBySlug(params.slug, [
    "title",
    "date",
    "slug",
    "author",
    "content",
    "ogImage",
    "coverImage",
  ]);
  const content = await markdownToHtml(post.content || "");

  return {
    props: {
      post: {
        ...post,
        content,
      },
    },
  };
}

export async function getStaticPaths() {
  const posts = getAllPosts(["slug"]);

  return {
    paths: posts.map((post) => {
      return {
        params: {
          slug: post.slug,
        },
      };
    }),
    fallback: false,
  };
}
