/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "theme-ui";
import { Container, Grid } from "theme-ui";
import SectionHeader from "../components/section-header";
import TeamCard from "../components/team-card";
import { FaFacebookF, FaTwitter, FaInstagram } from "react-icons/fa";

import Member1 from "../assets/team/aya.jpg";
import Member2 from "../assets/team/siya.jpg";
import Member3 from "../assets/team/tally.jpg";

const data = [
  {
    id: 1,
    imgSrc: Member1,
    altText: "Ayabonga Qwabi",
    title: "Ayabonga Qwabi",
    designation: "Software Craftsman",
    about: `Ayabonga is a visionary software developer with a knack for turning complex challenges into elegant solutions. With a wealth of experience in crafting robust code and a keen eye for detail, Ayabonga ensures that every line of code contributes to exceptional digital experiences.`,
    socialProfile: [
      {
        id: 1,
        name: "facebook",
        path: "#",
        icon: <FaFacebookF />,
      },
      {
        id: 2,
        name: "twitter",
        path: "#",
        icon: <FaTwitter />,
      },
      {
        id: 3,
        name: "instagram",
        path: "#",
        icon: <FaInstagram />,
      },
    ],
  },
  {
    id: 1,
    imgSrc: Member2,
    altText: "Siyamthanda Nomgqokwana",
    title: "Siyamthanda Nomgqokwana",
    about: `Siyamthanda is a tech trailblazer who combines technical prowess with a deep understanding of IT infrastructure. With a passion for optimizing systems and an eye for identifying opportunities for enhancement, Siyamthanda plays a pivotal role in elevating the technological landscape of our projects.`,
    designation: "Tech Enthusiast and IT Specialist",
    socialProfile: [
      {
        id: 1,
        name: "facebook",
        path: "#",
        icon: <FaFacebookF />,
      },
      {
        id: 2,
        name: "twitter",
        path: "#",
        icon: <FaTwitter />,
      },
      {
        id: 3,
        name: "instagram",
        path: "#",
        icon: <FaInstagram />,
      },
    ],
  },
];

export default function TeamSection() {
  return (
    <section>
      <Container sx={styles.container}>
        <SectionHeader slogan="" title="Meet Our Dynamic Team" />

        <Grid sx={styles.grid}>
          {data.map((item) => (
            <TeamCard
              key={`team--key${item.id}`}
              src={item.imgSrc}
              altText={item.altText}
              title={item.title}
              designation={item.designation}
              social={item.socialProfile}
              about={item.about}
            />
          ))}
        </Grid>
      </Container>
    </section>
  );
}

const styles = {
  container: {
    mt: [6, null, null, 7],
  },
  grid: {
    mt: [0, null, -6, null, -4],
    gridGap: ["35px 0px", null, 0, null, null, "30px 35px"],
    gridTemplateColumns: [
      "repeat(1,1fr)",
      null,
      "repeat(2,1fr)",
      "repeat(2,1fr)",
      null,
      "repeat(2,1fr)",
    ],
  },
};
