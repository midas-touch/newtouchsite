/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from 'theme-ui';
import { Container, Grid } from 'theme-ui';
import SectionHeader from '../components/section-header';
import FeatureCardColumn from '../components/feature-card-column.js';
import Performance from '../assets/key-feature/performance.svg';
import Partnership from '../assets/key-feature/partnership.svg';
import Subscription from '../assets/key-feature/subscription.svg';
import Support from '../assets/key-feature/support.svg';

const data = [
  {
    id: 1,
    imgSrc: Performance,
    altText: 'Perfomant Applications',
    title: 'Perfomant Applications',
    text:
      'Have you ever used a government website? do you know how slow they are? Our web apps use the latest cutting edge software to deliver a fast and reliable experience for your users. ',
  },
  {
    id: 2,
    imgSrc: Partnership,
    altText: 'Business Essential',
    title: 'Business Essential',
    text:
      'Our web apps intergrate with your business core functionality to deliver an authentic experience for your users. Functionality such as payments, tracking, audits and vital business analytics.',
  },
  {
    id: 3,
    imgSrc: Subscription,
    altText: 'Premium Service',
    title: 'Premium Service',
    text:
      'Because we use the latest technology, with us your business has access to premium services such as real-time communication, zero server delays and excellent data analytics.',
  },
  {
    id: 4,
    imgSrc: Support,
    altText: 'Excellent Maintanance',
    title: 'Excellent Maintanance',
    text:
      'We take maintanance seriously. Our web apps are monitored 24 hours 7 days a week to ensure a smooth customer experience. Should any issues arise they get handled immediately and effectively.',
  },
];

export default function KeyFeature() {
  return (
    <section sx={{ variant: 'section.keyFeature' }} name="feature">
      <Container>
        <SectionHeader
          slogan="So what do you offer?"
          title="Key features of our Web Applications"
        />

        <Grid sx={styles.grid}>
          {data.map((item) => (
            <FeatureCardColumn
              key={item.id}
              src={item.imgSrc}
              alt={item.altText}
              title={item.title}
              text={item.text}
            />
          ))}
        </Grid>
      </Container>
    </section>
  );
}

const styles = {
  grid: {
    width: ['100%', '80%', '100%'],
    mx: 'auto',
    gridGap: [
      '35px 0',
      null,
      '40px 40px',
      '50px 60px',
      '30px',
      '50px 40px',
      '55px 90px',
    ],
    gridTemplateColumns: [
      'repeat(1,1fr)',
      null,
      'repeat(2,1fr)',
      null,
      'repeat(4,1fr)',
    ],
  },
};
