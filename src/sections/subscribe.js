/** @jsxRuntime classic */
/** @jsx jsx */
import { useEffect, useRef, useState } from "react";
import fetch from "isomorphic-unfetch";
import React from "react";
import { jsx } from "theme-ui";
import {
  Container,
  Flex,
  Box,
  Button,
  Input,
  Radio,
  Heading,
  Paragraph,
} from "theme-ui";
import { animateScroll } from "react-scroll";

const encode = (data) => {
  return Object.keys(data)
    .map((key) => encodeURIComponent(key) + "=" + encodeURIComponent(data[key]))
    .join("&");
};

export default function Subscribe() {
  const [name, setName] = useState("");
  const [botField, setBotField] = useState(null);
  const [email, setEmail] = useState("");
  const [cell, setCell] = useState("");
  const [selectedPackage, setPackage] = useState("");
  const [hasSubmitted, setHasSubmitted] = useState(false);

  const handleSubmit = (e) => {
    let data = { name, email, cell, selectedPackage };
    if (botField === null) {
      fetch("https://hooks.zapier.com/hooks/catch/4831825/b58hn88/", {
        method: "POST",
        headers: { "Content-Type": "application/x-www-form-urlencoded" },
        body: encode({ ...data }),
      })
        .then(() => setHasSubmitted(true))
        .catch((error) => alert(error));
    }

    e.preventDefault();
  };

  const handleChange = (e) => {
    const setters = {
      email: setEmail,
      name: setName,
      cell: setCell,
      "bot-field": setBotField,
    };
    setters[e.target.name](e.target.value);
  };

  useEffect(() => {
    if (hasSubmitted === true) {
      setTimeout(() => {
        animateScroll.scrollMore(-250);
      }, 200);
    }
  });
  return (
    <section name="contact" id="contact">
      <Container>
        <Box sx={styles.contentBox}>
          <Box sx={styles.contentBoxInner}>
            {hasSubmitted && (
              <Heading as="h2" sx={styles.title}>
                Great ! your details have been submitted successfully.
              </Heading>
            )}

            {!hasSubmitted && (
              <>
                <Heading as="h2" sx={styles.title}>
                  Let's drop you a call
                </Heading>
                <Paragraph>
                  Embark on a journey of digital transformation with Midas Touch
                  Technologies. Whether you're a startup, a growing SME, or an
                  established business, our custom solutions are tailored to
                  your needs. Get in touch today to explore how we can turn your
                  digital dreams into reality.
                  <br />
                </Paragraph>
                <form name="contact" onSubmit={handleSubmit}>
                  <input type="hidden" name="form-name" value="contact" />
                  <p hidden>
                    <label>
                      Don’t fill this out:{" "}
                      <input name="bot-field" onChange={handleChange} />
                    </label>
                  </p>
                  <Flex sx={styles.subscribeForm}>
                    <Input
                      id="fullname"
                      name="name"
                      type="text"
                      placeholder="Enter your full name"
                      onChange={handleChange}
                      value={name}
                    />
                    <Input
                      id="email"
                      name="email"
                      type="text"
                      placeholder="Enter your email address"
                      onChange={handleChange}
                      value={email}
                    />
                    <Input
                      id="cell"
                      name="cell"
                      type="text"
                      placeholder="Enter your cellphone number"
                      onChange={handleChange}
                      value={cell}
                    />
                    <Button
                      type="submit"
                      className="subscribe__btn"
                      aria-label="Submit"
                    >
                      Submit
                    </Button>
                  </Flex>
                </form>
              </>
            )}
          </Box>
        </Box>
      </Container>
    </section>
  );
}

const styles = {
  buttons: {
    width: "100%",
    display: "flex",
    flexDirection: ["column", "row"],
    justifyContent: "space-evenly",
    alignItems: "center",
    input: {
      width: "20px",
    },
    label: {
      color: "primary",
    },
  },
  contentBox: {
    backgroundColor: "primary",
    textAlign: "center",
    borderRadius: 10,
    py: ["60px", null, 8],
    marginTop: "55px",
  },
  contentBoxInner: {
    width: ["100%", null, "540px", "600px"],
    mx: "auto",
    mt: -1,
    px: [3, 5],
    color: "white",
  },
  title: {
    fontSize: ["24px", null, "28px", null, null, "32px", null, "36px"],
    color: "white",
    lineHeight: [1.3, null, null, 1.25],
    fontWeight: "700",
    letterSpacing: ["-.5px", null, "-1.5px"],
    mb: [2, 3],
  },
  description: {
    fontSize: ["15px", 2, null, null, null, "17px", null, 3],
    color: "white",
    lineHeight: [1.85, null, null, 2],
    px: [0, null, 5],
  },
  subscribeForm: {
    mt: [6, null, null, 7],
    backgroundColor: ["transparent"],
    borderRadius: 5,
    overflow: "hidden",
    padding: "15px",
    flexDirection: ["column"],
    justifyContent: "center",
    alignItems: "center",
    label: {
      color: "white !important",
    },
    input: {
      borderBottom: "1px solid #eb4b602e",
      fontFamily: "body",
      fontSize: ["14px", null, 2],
      fontWeight: 500,
      color: "heading",
      py: 1,
      px: [4, null, 6],
      backgroundColor: ["white"],
      marginBottom: "15px",
      height: ["52px", null, "60px"],
      textAlign: ["center", "left"],
      "&:focus": {
        boxShadow: "0 0 0 0px",
      },
      "::placeholder": {
        color: "primary",
        opacity: 1,
      },
    },
    ".subscribe__btn": {
      width: ["50%", "30%"],
      flexShrink: 0,
      ml: [0, 2],
      backgroundColor: ["text", "white"],
      color: "primary",
      mt: [2, 0],
      py: ["15px"],
    },
  },
};
