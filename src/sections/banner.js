/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "theme-ui";
import { Container, Box, Heading, Text, Image, Button } from "theme-ui";
import BannerImg from "../assets/web_development_2.gif";
import SponsorImage from "../components/image";
import { Link } from "react-scroll";
import img1 from "../assets/sponsor/clinicplus.svg";
import img2 from "../assets/sponsor/logo.jpg";
import img3 from "../assets/sponsor/tecla.png";
import img4 from "../assets/sponsor/vitanurse.png";

export default function Banner() {
  return (
    <section sx={styles.banner} id="home">
      <Container sx={styles.banner.container}>
        <Box sx={styles.banner.contentBox}>
          <Heading as="h1" variant="heroPrimary">
            User-Centric Web Solutions for Businesses
          </Heading>
          <Text as="p" variant="heroSecondary">
            We're passionate about driving business success through web
            applications designed with the user in mind. Our solutions
            seamlessly integrate across devices, ensuring your customers'
            experience is always exceptional.
          </Text>

          <Link
            to={"contact"}
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
          >
            <Button variant="primary">Get a quote</Button>
          </Link>
        </Box>

        <Box sx={styles.banner.imageBox}></Box>
      </Container>
    </section>
  );
}

const styles = {
  partner: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    filter: "hue-rotate(395deg)",
    fontWeight: "bold",
    mt: "20px",
    mb: ["40px"],
    "> div + div": {
      ml: ["10px", null, null, "20px", null, "30px"],
    },
    img: {
      display: "flex",
    },
    span: {
      fontSize: [1, null, null, null, 2],
      color: "#566272",
      lineHeight: [1],
      opacity: 0.6,
      display: "block",
      mb: ["20px", null, null, null, "0px"],
      mr: [null, null, null, null, "20px"],
      flex: ["0 0 100%", null, null, null, "0 0 auto"],
    },
  },
  banner: {
    pt: ["140px", "145px", "155px", "170px", null, null, "180px", "215px"],
    pb: [2, null, 0, null, 2, 0, null, 5],
    position: "relative",
    zIndex: 2,
    "&::before": {
      position: "absolute",
      content: '""',
      bottom: 6,
      left: 0,
      height: "100%",
      width: "100%",
      zIndex: -1,
      backgroundRepeat: `no-repeat`,
      backgroundPosition: "bottom left",
      backgroundSize: "36%",
    },
    "&::after": {
      position: "absolute",
      content: '""',
      bottom: "40px",
      right: 0,
      height: "100%",
      width: "100%",
      zIndex: -1,
      backgroundRepeat: `no-repeat`,
      backgroundPosition: "bottom right",
      backgroundSize: "32%",
    },
    container: {
      minHeight: "inherit",
      display: "flex",
      flexDirection: ["column", "row"],
      justifyContent: "center",
      marginBottom: "12vh",
    },
    contentBox: {
      width: ["100%", "90%", "535px", null, "57%", "60%", "68%", "60%"],
      mx: "auto",
      textAlign: "center",
      mb: ["40px", null, null, null, null, 7],
    },
    imageBox: {
      width: ["100%", "500px"],
      height: ["200px", "500px"],
      justifyContent: "center",
      textAlign: "center",
      display: "inline-flex",
      mb: [0, null, -6, null, null, "-40px", null, -3],
      backgroundImage: `url(${BannerImg})`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "contain",
      backgroundPosition: "center",
    },
  },
};
