---
title: 'A South African guide to building your first NextJS website'
excerpt: 'This is a step by step guide to building your first with NextJS. NextJS is by far the most popular and highly perfomant static website generation tool. We will be styling our web app with styled components and host our domain on registerdomain.co.za'
coverImage: '/assets/blog/sa-way.jpg'
date: '2022-02-05T16:35:07.322Z'
author:
  name: Ayabonga Qwabi
  picture: '/assets/blog/authors/aya.jpg'
ogImage:
  url: '/assets/blog/sa-way.jpg'
---
 
In this tutorial, we will be giving you a step-by-step guide to how you can build your first website with NextJS which you can deploy to Netlify.
 
Before we get started these are some of the tools we will need for this tutorial.
#### 1. A [Gitlab](https://www.gitlab.com) Account 

For this tutorial, we will be using Gitlab as opposed to the infamous Github for our source control. Gitlab is pretty much the same as Github. The major difference is that Gitlab gives us wider access to DevOps tools and is more Enterprise focused.

#### 2. A [Netlify](https://www.netlify.com) account 

As opposed to Vercel in this tutorial we will be using Netlify to host our site. I chose Netlify because of the easy DNS configuration setup and its great plugin and feature support. I also chose Netlify because it is very Enterprise focused.

#### 3. A Linux machine 

#### 4. A [Register Domain](https://www.registerdomain.co.za) account 

If you're looking for an easy way to get cheap website domains in South Africa www.registerdomain.co.za is your best option. So head over there and create your account and buy your first website domain.

#### 5. NodeJS version 14 or higher 
You will need to install NodeJS 14 or any latest version. I chose version 14 because it works well with the NextJS V12 engine.

 ### [Create Next App](https://create-next-app.js.org/)

Let's start by using Create Next App because it's an easy way of getting a NextJS bootstrap website for us. Create Next App will give you a list of templates that you can use to start your site. To install create-next-app you need to run

```
  npm install --global @create-next-app/core
```

To create your first website run

```
 create-next-app my-app --template default
```

This should create a folder called `my-app` which you can cd into. 

To start your website run the npm dev script which is defined on the folder's package.json
```
 npm run dev
```


You may see a couple of errors on the terminal but don't worry 
To fix these simply rename the static folder to public  and edit the file `next.config.js` to this 

```
module.exports = {
  webpack: (config) => {
    return config
  }
};
```

These errors may be because the current version of create-react-app has not been synced yet with the latest NextJS engine.

If you open the following URL http://localhost:3000 on your browser you should see your website running on port 3000.

### Styled Components

So now that you have your bootstrap running you may want to customize your website your way and add your styling.

You can use this wonderful CSS-in-js library called styled-components to style your website.

```
npm install --save styled-components
```

Then we also need its babel plugin which you can install by running:

```
npm install --save-dev babel-plugin-styled-components
```

Next you will need to add a `.babelrc` file to the root of your directory with the following contents

```
{ 
  "presets": ["next/babel"], 
  "plugins": [
      ["styled-components", { "ssr": true }]
   ]
}
```

We will need to create our own custom `_document.js` page which is going to inject our js styles into our website , so inside the pages folder create the following page `_document.js` with the following content

```
import Document, { Html, Head, title, Main, NextScript } from 'next/document';
// Import ServerStyleSheet from styled components 
import { ServerStyleSheet } from 'styled-components';

export default class MyDocument extends Document {
  static getInitialProps({ renderPage }) {
    // Step 1: Create a new instance of a ServerStyleSheet
    const sheet = new ServerStyleSheet();

    // Step 2: Collect all styles used in the App
    const page = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />),
    );

    // Step 3: Extract these styles to style sheet tags
    const styleTags = sheet.getStyleElement();

    // Step 4: Pass styleTags as a prop to the page
    return { ...page, styleTags };
  }

  render() {
    return (
        <Html lang="en-US">
        <Head>
          {/* Step 5: Render the style tags in the head  */}
          {this.props.styleTags}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
```
Now we can add JS styles to our code. Let's edit the home page to our own thing.
Edit the `pages/index.js` to look like this.

```
import Head from '../components/head';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 95vh;
  margin: 0px;
  background: ${props => props.background};
`;

const Heading = styled.h1`
  font-size: 30px;
  font-family: sans-serif;
  font-weight: 900;
`;

export default () => (
  <div>
    <Head title="My website" />
    <Container background="#d3d3d31f">
      <Heading>
        Hey guys, welcome to my website!
      </Heading>       
    </Container>  
  </div>
);
```
If you notice, we added Container and Header components which we created using styled-components. 

Styled components offer us a range of styling features for websites such, as theming, global styles, passed props, and much more.

#### Some more on NextJS sites

There are a lot of features that I am not going to go through in this tutorial but you can find links for them below

1. [Serving static images](https://github.com/cyrilwanner/next-optimized-images)
2. [Adding a blog](https://www.netlify.com/blog/2020/05/04/building-a-markdown-blog-with-next-9.4-and-netlify/)
3. [Page Routing](https://nextjs.org/docs/routing/introduction)
4. [Dynamic Pages](https://nextjs.org/learn/basics/dynamic-routes)
5. [Adding a server](https://nextjs.org/docs/advanced-features/custom-server)

### Deploying to Netlify

Now that we have our website, we're ready to deploy to netlify.
First, we will create a Netlify configuration file. In the root directory of your website create the  file `netlify.toml` with the following contents

```
  [build]
    command = "npm run build && npm run export"
    publish = "out"
```

Check your package.json to make sure it has the following scripts
```
...

"scripts": {
    "dev": "next",
    "build": "next build",
    "start": "next start"
    "export": "next export"
  }

...
```

Log in or create an account on [Gitlab](https://www.gitlab.com) and then create a blank repository.
In your terminal initialize your website folder as a git repository and make your first commit
```
git init
git add .
git commit -"my first commit"
```
Then add your Gitlab repo as a remote
```
git remote add origin https://gitlab.com/[YOUR_USERNAME]/[YOUR_REPO].git

```
e.g

```
git remote add origin https://gitlab.com/ayabongaqwabi1/my-app.git
```
Then push your repo to Gitlab
```
 git push -u origin master
```

Head over to [Netlify](https://www.netlify.com) and log in with your Gitlab account. 
- Go to Sites
- Add new Site
- Import an existing project from a Git repository
- Select Gitlab
- Select Repo
- Select Master branch
- Then click Deploy

If you encounter the following error while deploying your website
```
Error: The directory "out" does not contain a Next.js production build. 
Perhaps the build command was not run, or you specified the wrong publish directory.
```
 
 1. Go to your site settings on Netlify
 2. Go to Build & Deploy
 3. Go to Environment
 4. Add a new environment variable `NETLIFY_NEXT_PLUGIN_SKIP` and set it to true
 5. Go to Deploy setting 
 6. Trigger another deploy

 There you go your site is now Live on Netlify. My site lives at [https://epic-cray-34f39a.netlify.app/](https://epic-cray-34f39a.netlify.app/)

 ### Adding a domain to your website

 Now we need to head over to registerdomain and buy a new domain.

 Once you have created your domain
 1. Click Domains
 2. Go to your domain settings
 3. Click on nameservers
 4. Replace Register Domain's name servers with [Netlify's](https://docs.netlify.com/domains-https/netlify-dns/delegate-to-netlify)


> * dns1.p04.nsone.net
> * dns2.p04.nsone.net
> * dns3.p04.nsone.net
> * dns4.p04.nsone.net

Next, head over to your Netlify site's settings

1. Select Domain Management
2. Add a new domain

aaaand done! Your new site should now live on your domain. Mine lives at [www.my-app.touch.net.za](http://my-app.touch.net.za)

Just a note though it may take around 2- 4 hours for DNS configurations to propagate so your website may not instantly be live on your domain. Don't panic just wait a bit.

Thank you for reading :) I hope you learned something new today, till next time!









