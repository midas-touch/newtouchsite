---
title: ' Deploying a Next.js App on AWS EC2: A Comprehensive Guide'
excerpt: 'Learn how to deploy your Next.js app on AWS EC2 with this step-by-step tutorial. Configure a reverse proxy, adjust firewall settings, and secure your app for optimal performance.'
coverImage: '/assets/blog/deploy.png'
date: 'Thu Sep 14 2023 14:11:35 GMT+0200'
author:
  name: Ayabonga Qwabi
  picture: '/assets/blog/authors/aya.jpg'
ogImage:
  url: '/assets/blog/deploy.png'
---
**Deploying a Next.js App on AWS EC2: A Step-by-Step Guide**

This guide will walk you through the process step by step, from setting up the EC2 instance to configuring a reverse proxy and adjusting the firewall rules. By the end of this tutorial, you'll have your Next.js app up and running securely on AWS.

### Prerequisites

Before we dive into the deployment process, make sure you have the following:

1. **AWS Account**: You'll need an AWS account to create and manage EC2 instances and other AWS resources.

2. **Next.js App**: Have your Next.js app's source code ready. You should be able to clone it from a version control repository like Git.

### Step 1: Launch an EC2 Instance

1. Log in to your AWS Management Console and navigate to the EC2 service.

2. Click the "Launch Instance" button and choose an appropriate Amazon Machine Image (AMI) for your EC2 instance. A Linux-based AMI is a common choice.

3. Select an instance type based on your app's requirements and click "Next" to configure instance details, storage, and tags. Follow the prompts to complete the instance setup.

### Step 2: Configure Security Groups

1. During the EC2 instance setup, you'll reach the "Configure Security Group" step. Here, you need to configure the firewall rules for your instance.

2. Create a security group or choose an existing one. Ensure that it allows inbound traffic on the necessary ports. For a Next.js app, you'll likely need to allow HTTP (port 80) and HTTPS (port 443) traffic. Adjust the rules as needed.

### Step 3: SSH into Your EC2 Instance

1. Once your EC2 instance is up and running, use SSH to connect to it. Replace `your-key-pair.pem` with your SSH key pair and `your-ec2-instance-ip` with your EC2 instance's public IP address:

   ```bash
   ssh -i your-key-pair.pem ec2-user@your-ec2-instance-ip
   ```
2. If you don't have access to your keypair you can always ssh into your instance from the console

Connect to your instance by clicking the connect button

![Connect](/assets/blog/c1.png)

Choose default ssh connection

![Defaults](/assets/blog/c2.png)


### Step 4: Install Node.js and NPM

1. Inside the EC2 instance, install Nvm, Node.js and npm. Here's how to do it for Ubuntu:

   ```bash
   curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash 
   ```

   Export nvm home directory
   ```
   export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
   ```

  Install NodeJS and Npm

  ```nvm install node```

### Step 5: Clone Your Next.js App

1. Clone your Next.js app's source code from your version control system onto the EC2 instance. For example, using Git:

   ```
   git clone my-next-app-repo-url
   ```

### Step 6: Install Dependencies and Build Your App

1. Navigate to your project directory on the EC2 instance and install the project's dependencies:

   ```
    cd my-next-appp
    npm install
   ```

2. Build your Next.js app:

   ```bash
   npm run build
   ```

### Step 7: Set Up a Reverse Proxy with Nginx

1. Install Nginx on your EC2 instance:

   ```bash
   sudo apt-get install nginx
   ```

2. Create an Nginx configuration file for your Next.js app. Replace `your-domain.com` with your domain name or IP address and `your-next-app` with your app's name:

   ```bash
   sudo nano /etc/nginx/sites-available/my-next-app
   ```

3. Add the Nginx configuration. Here's a basic example:

```
   server {
    listen 80;
    listen [::]:80;
    server_name your-domain www.your-domain.com ; # Replace with your domain name or EC2 instance public IP address

      location / {
          proxy_pass http://localhost:3000; # Assuming your Next.js app is running on port 3000
          proxy_http_version 1.1;
          proxy_set_header Upgrade $http_upgrade;
          proxy_set_header Connection 'upgrade';
          proxy_set_header Host $host;
          proxy_cache_bypass $http_upgrade;
      }
  }
```

4. Enable the Nginx configuration:

   ```bash
   sudo ln -s /etc/nginx/sites-available/my-next-app /etc/nginx/sites-enabled/
   ```

5. Test the Nginx configuration:

   ```bash
   sudo nginx -t
   ```

6. Restart Nginx to apply the configuration:

   ```bash
   sudo systemctl restart nginx
   ```

### Step 8: Adjust Firewall Rules

To adjust the firewall rules, go back to your EC2 instance's security group settings.

#### Here's how you can do that

1. **Log in to AWS Console**:

   Log in to your AWS Management Console with your AWS account credentials.

2. **Navigate to EC2 Dashboard**:

   Go to the EC2 Dashboard by clicking on the "Services" dropdown in the top left corner and selecting "EC2" under the "Compute" section.

3. **Select Your EC2 Instance**:

   In the left menu select Instances and then locate and select the EC2 instance for which you want to adjust the firewall rules.
   ![Defaults](/assets/blog/c7.png)

4. **View Security Groups**:

   - In the left menu select under Network & Security select Security Groups
   - Locate and select the security group for your instance

![Defaults](/assets/blog/c3.png)

5. **Add or Edit Rules**:

   To add a new rule, click the "Edit inbound rules" button. You can then add or modify rules to allow traffic on specific ports or from specific IP addresses.

   For example, to allow HTTP traffic on port 80, you can add a rule like this:

   - Type: HTTP
   - Protocol: TCP
   - Port Range: 80
   - Source: 0.0.0.0/0 (This allows traffic from any source IP, but you can restrict it to a specific IP range if needed)

   Make sure to adjust the rules according to your application's requirements. You can also allow traffic on other ports like HTTPS (443), SSH (22), or custom ports for your application.

   ![Defaults](/assets/blog/c4.png)

   

7. **Save Rules**:

   After adding or modifying rules, click the "Save rules" button to apply the changes to the security group.

![Defaults](/assets/blog/c5.png)

#### Configure Internal firewall

You need to set firewall rules using the firewall service ufw to allow web access to the Nginx server. View a list of firewall configurations with the following command:

```sudo ufw app list```

You should get this output:

```
Available applications:
  Nginx Full
  Nginx HTTP
  Nginx HTTPS
  OpenSSH
```

To allow HTTP traffic, use the actual profile name — rather than the value 80 for HTTP and 443 for HTTPS. Run the command:

```sudo ufw allow 'Nginx HTTP'```

```sudo ufw allow OpenSSH```

Verify that the configuration is active:

```sudo ufw status```

This should be the output:

```
Status: active
To                         Action      From
--                         ------      ----
OpenSSH                    ALLOW       Anywhere                  
Nginx HTTP                 ALLOW       Anywhere                  
OpenSSH (v6)               ALLOW       Anywhere (v6)             
Nginx HTTP (v6)            ALLOW       Anywhere (v6)
```

> If your output says status:inactive, run the command sudo ufw enable

Run the following commands to restart the server (for the changes to take effect) and check the status of the Nginx server:

```sudo systemctl restart nginx```
```sudo systemctl status nginx```


### Step 9: DNS Configuration (Optional)

If you have a domain name, configure your DNS settings to point to your EC2 instance's IP address.

### Step 10: Secure Your Server (Optional)

Implement SSL/TLS certificates for HTTPS to enhance security. You can use AWS ACM or Let's Encrypt for this purpose.

#### To use Let's Encrypt do
1. Install cerbot and its plugins:


```sudo apt install certbot python3-certbot-nginx```

2. Obtain an SSL certificate by performing the command below:

```sudo certbot --nginx -d your-domain.com -d www.your-domain.com```

Similar to the other sections in this article, replace your-domain.com by your own domain. Never forget it!

After a few seconds, you will see this:

```
IMPORTANT NOTES:
Successfully received certificate.
Certificate is saved at: /etc/letsencrypt/live/your-domain.com/fullchain.pem
Key is saved at: /etc/letsencrypt/live/your-domain.com/privkey.pem
```

This means you’ve succeeded. The final task is to restart your Nginx server:


```sudo sytemctl restart nginx```

Now you can see your Next.js app online with https instead of http:

```https://<your domain>```

### Aand we're done!



With these steps, your Next.js app is deployed on an AWS EC2 instance, configured with a reverse proxy, and secured with adjusted firewall rules. Your app is now accessible to the world, ready to serve users with speed and security. Happy deploying!

Thank you to these posts

[How to host react app with Nginx](https://enrico-portolan.medium.com/how-to-host-react-app-with-nginx-634ad0c8425a)

[How to deploy React App to Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-react-application-with-nginx-on-ubuntu-20-04)

[Deploying CRA with Nginx & Ubuntu ](https://medium.com/@timmykko/deploying-create-react-app-with-nginx-and-ubuntu-e6fe83c5e9e7)

[Running a NextJS App on Linux based EC2 instance](https://awstip.com/running-a-next-js-application-on-a-linux-based-ec2-instance-1b5bab9c6728)

