---
title: 'What is requirements engineering?'
excerpt: 'Before they can start bashing out code when Software developers undertake a project they first have to understand the needs of the and behaviours of the application to be developed. On this post we delvo deep into the process of requirements enineering.'
coverImage: '/assets/blog/requirements.png'
date: '2022-02-04T16:35:07.322Z'
author:
  name: Ayabonga Qwabi
  picture: '/assets/blog/authors/aya.jpg'
ogImage:
  url: '/assets/blog/requirements.png'
---

The requirements of a web application are the services, its functionality, and how it is supposed to respond to user interaction. The requirements are an outline of what the system is supposed to do and what it is not supposed to do. These requirements reflect the needs of our end-user. They reveal to us what the end-user is trying to use the application for.

What are they trying to achieve?

Are they trying to:
- Make a booking?
- Communicate with their friends?
- Find information?
- Request a certain service?
- Buy a certain product?

A requirement can either be an abstract description of how a web app is supposed to behave or it can be a detailed description of the way the app is supposed to respond to a certain interaction from the end-user.

Before developers can start the coding process, they need to understand what to build and how best to build it? When the requirements are specified they need to be specified in a manner that is specific and detailed. This is so that developers can plan on which tools, which coding approaches, which standards, and software architectural patterns to follow when they build the application.

By definition, there are two types of requirements.

### User Requirements

These are requirements where we use simple language, and in a non-technical manner, we thoroughly describe the various services our web app is supposed to deliver to the end-user. This type of requirement is meant to be easily understood by both technical and non-technical personnel. When we define this type of requirement we need to stay away from using any form of technical jargon. Nowadays technical gurus have figured out that the best way to construct user requirements with little to no technical jargon is through user stories.

An example of a user requirement maybe
- Our users should be able to make a booking to visit the doctor on a certain date
### System Requirements

These are requirements where we technically use formal language to thoroughly describe the various services and functionalities our web app is supposed to deliver to the end-user. A system requirement should define in detail what needs to happen in the background as the user interacts with our application. These are intended for the technical personnel within the project team. They can be written to form a specification document which can be provided alongside the contract to explain to the client what needs to be built and how long they may take to build. System requirements may also be used to design the overall architecture of the web application.

An example of a system requirement may be 
- The user needs to be provided with a button option to start the booking process.
- Once clicked the app needs to route to the booking details page where the user will be presented with a booking form that requests the following details Name, Surname, Ailment, and Date
- On form submission, an API request should be made to the server to store the user's details on the database

### Why is this process important?

These requirements are important because they alude to each project member what they will be working on. 

For example 
- A developer may be concerned with what components they need to build for the app
- The project owner might be concerned with how certain interactions may be unsatisfactory to the user
- The system architect may be concerned about how best the app can efficiently deliver certain functionalities.
- A UX designer may be concerned with how best to deliver certain interactions to the end-user visually.

They are often used to construct the Software Requirements Document, which we can chat about in my next post.

Thank you for reading I hope you learned something new today :)