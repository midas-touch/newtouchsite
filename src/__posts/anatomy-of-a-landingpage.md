---
title: 'Anatomy of the header of the home page'
excerpt: 'Learn about the fundamental structure of a home page. This guide will help you create beautiful aeasthetic designs for the web.'
coverImage: '/assets/blog/anatui.jpg'
date: '2022-02-20T16:35:07.322Z'
author:
  name: Ayabonga Qwabi
  picture: '/assets/blog/authors/aya.jpg'
ogImage:
  url: '/assets/blog/anatui.jpg'
---

As with any other typical HTML page, the landing section of a home page consists of three fundamental parts. The Header, the body, and the footer.

![Anatomy Example](/assets/blog/anatomy.png)

In this post, we're going to learn how we can start designing our websites by following this simple structure.
## The Header

The Header is by far one of the most interactive parts of the landing section as it contains the logo, the navigation bar, and secondary call to action elements. On the top left corner of your page, you typically find the logo of your website. The logo does not have to be on the left corner but as trend dictates that is where it should be. The other reason for this has to do with how our user's attention gets controlled when they first land on a website. Studies have found that the first place a user will look when they visit your website is at the top right corner. This is why more important call-to-action elements otherwise known as ctas are placed in the top right corner. CTA are elements that allow the user to perform a specific action, such as sign up, sign in, buy or even subscribe. To put it simply they call a user to act.

Here's an example of a simple header

![Header example 1](/assets/blog/header1.png)

## The Body

The body is the most content-heavy part of our landing section which contains text, images, and other abstract elements. The body is the part of the page where designers enjoy showing their abilities on. The body can either be structured left to right or right to left. It is commonly split into two parts, the text containing primary cta elements and an illustration. The text section of the body usually contains a mega header followed by a blob of text.

Here are a couple of examples of a simple body

![Body Example 1](/assets/blog/body1.jpg)

![Body Example 2](/assets/blog/body2.jpg)

![Body Example 3](/assets/blog/body3.jpg)

## The Footer 

The footer is an optional part of the landing section and usually contains nothing. It is really up to the imagination of the designer on what to put in this section.

Designers typically put a list of elements for in-depth illustration on this section, lists such as sponsors, available products, or partners.

Here's an example of how this can be applied.

![Footer Example 3](/assets/blog/footer1.jpg)


Thank you for reading :) I hope you learned something new today, till next time!

Here are some complete examples

![Body Example 3](/assets/blog/example3.jpg)
![Body Example 3](/assets/blog/example2.jpg)