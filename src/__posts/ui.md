---
title: '4 UI Principles for winning designs'
excerpt: 'With the help of these 4 principles you can elevate you web design game to the next level. Read this blog post to understand what it means to create meaningful UI design. UI is more than just decoration, we design to solve problems not decorate. We say no to ornamentation and yes to problem solving.'
coverImage: '/assets/blog/ui2.jpg'
date: '2022-02-14T16:35:07.322Z'
author:
  name: Ayabonga Qwabi
  picture: '/assets/blog/authors/aya.jpg'
ogImage:
  url: '/assets/blog/ui.jpg'
---
## 1. UI design as a problem solving mechanism

It is a widely known fact that most UI designers create designs to be visually appealing or in line with the current trends. This may tempt aspiring UI designers to branch off in the wrong direction. The first thing we need to understand about design is that design is not just about aesthetic appearances. UI design is a problem-solving mechanism meaning when we create designs we need to try and solve a specific user problem. 

Some of the questions we may need to ask ourselves when creating UI designs are:
- Does the design have a purpose?
- What does it do?
- How should our users perceive this design?
- Why is this design important?

A design needs to go beyond just looking pretty and aesthetic. It needs to perform a certain function, it needs to solve a problem. UI design is not decoration.
We need to have a purpose for the visual decisions we make.

## 2. Solve your own problems

Most UI designers often go to the Internet and visit sites like Behance,  Dribble, or Deviant art with hopes of finding inspiration for their next design. On the contrary, we should try not to do this. We earlier said that UI design is problem-solving so when we look at other people's designs trying to find inspiration we are looking at the solutions they made for their problems and when we take these designs and apply them to our UI we're trying to solve their problem instead of ours.

In principle, we should not adopt design solutions without considering whether the solutions are appropriate for our problems. As a UI designer, you need to understand why you make the visual decisions you make.

## 3. Form over Function

Most designers believe that a design should be created by the functionality of the product. They believe that function takes more priority than how the design looks and not the other way around. This is not entirely true. Great Ui is created from seamless integration between form and function.

When the function is prioritized over the form we end up building a developer-centric product that doesn't address the needs of the end-user. A popular example of this would be the Pied Piper program from the infamous tv series Silicon Valley. The program Richard and his friends built solved a major file compression problem, it was mind-blowing and the pinnacle of Silicon Valley discoveries. With all the great features in the file compression industry, it couldn't keep a committed user base. Why? because the UI was more focused on what it can do other than how it communicates to the user. It was developer-centric. Made by developers with no understanding of core UI concepts.

The form can be derived from a variety of considerations such as
- What do our users need?
- How should this be easily accessible to all users?
- What functionality should it provide?
- What message should it communicate to our users 
- How well does it look?
- How much time do we need to produce this?
- How many people do we have to produce this?
- How long will it take to produce this?
- How much are we budgeted for this?

The form should answer all of these questions for us. All of these need to be taken into consideration when creating a UI design. As a result, if all of these are considered, form, therefore, takes priority over function.

## 4. Less complexity

When we create our design, ideally we should try as much as we can to make things as simple as possible. We should simplify our designs soo much that they become dumb to us. They need to be dumb so that any user who attempts to use a working model of our designs doesn't experience any frustrations. Dumb designs are a great way of keeping our users engaged.
Although they shouldn't just be dumb. They should be simple, dumb, and effective. We should always try and avoid going to the extremes whereby our designs are so simple they don't make sense to the average user.

Our users should not have to work hard trying to understand how our interfaces work. We should not expect our users to be UI experts. We should ideally assume that every user who uses our design is dumb or it's their first time using an app or website.

Thank you for reading, I hope you learned something today :)

